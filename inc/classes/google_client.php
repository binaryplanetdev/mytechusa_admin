<?php
	$client = new Google_Client();
	$client->setAuthConfigFile($_SERVER["DOCUMENT_ROOT"] . "/GoogleApi/client_secrets.json");
	$client->setHostedDomain($HOST_DOMAIN);
	
	$client->addScope(Google_Service_Directory::ADMIN_DIRECTORY_GROUP);
	$client->addScope(Google_Service_Directory::ADMIN_DIRECTORY_USER);
	$client->addScope(Google_Service_Directory::ADMIN_DIRECTORY_GROUP_MEMBER);
	$client->addScope(Google_Service_Directory::ADMIN_DIRECTORY_USER_ALIAS);
	$client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
	$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
	$client->addScope("https://apps-apis.google.com/a/feeds/emailsettings/2.0/");
	
	if(isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) {
		$client->setAccessToken($_SESSION['access_token']);
	}
?>