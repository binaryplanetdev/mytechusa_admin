<?php
	class CGroupManager {
		var $mysql;
		var $google_client;
		var $directory_service;
		
		var $local_group_list;
		var $local_store_list;
		var $google_group_list;
		var $is_google_group_json_format;
		var $is_local_store_json_format;
		
		var $default_option;
		var $max_result_count = 500;
		
		function CGroupManager($_google_client, $_mysql) {
			$this->google_client = $_google_client;
			$this->directory_service = new Google_Service_Directory($this->google_client);
			$this->mysql = $_mysql;
			
			$this->is_google_group_json_format = false;
			$this->is_local_store_json_format = false;
			$this->google_group_list = array();
			$this->local_group_list = array();
			$this->local_store_list = array();
			$this->default_option = array('customer' => 'my_customer', "maxResults" => $this->max_result_count);
		}
		
		function getLocalGroupList() {
			return $this->local_group_list;
		}
		
		function getLocalGroupListAsJson() {
			if(count($this->local_group_list) > 0) {
				return json_encode($this->local_group_list);
			} else {
				return "[]";
			}
		}
		
		function getListLocalGroups() {
			try {
				$this->local_group_list = array();
				$local_group_list = $this->mysql->get("google_groups");
					
				if($this->mysql->count <= 0) {
					return;
				}
					
				foreach ($local_group_list as $group) {
					if(!isset($this->local_group_list[$group["google_groups_id"]])) {
						$this->local_group_list[$group["google_groups_id"]] = array();
					}
						
					$this->local_group_list[$group["google_groups_id"]] = $group;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get local group list; getListLocalGroups(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function getGoogleGroupList() {
			return $this->google_group_list;
		}
		
		function getGoogleGroupListAsJson() {
			try {
				if(count($this->google_group_list) > 0) {
					if(!$this->is_google_group_json_format) {
						$this->makeGoogleGroupListAsJsonFormat();
					}
				
					return json_encode($this->google_group_list);
				} else {
					return "[]";
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get google group list as json; getGoogleGroupListAsJson(); ERROR[" . $e->getMessage() . "]");
				return "[]";
			}
		}
		
		function makeGoogleGroupListAsJsonFormat() {
			try {
				$groups = array();
				foreach($this->google_group_list as $group) {
					if(!isset($groups[$group["id"]])) {
						$groups[$group["id"]] = array();
					}
									
					$groups[$group["id"]] = $group;
				}
					
				$this->google_group_list = $groups;
				$this->is_google_group_json_format = true;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to make google group list as json format; makeGoogleGroupListAsJsonFormat(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function getListGoogleGroups($_option = null) {
			try {
				if(!isset($_option)) {
					$this->google_group_list = array();
					$_option = $this->default_option;
				}
					
				$directory_group = $this->directory_service->groups->listGroups($_option);
				$tmp_group_list = $directory_group->getGroups();
				
				$this->google_group_list = array_merge($this->google_group_list, $tmp_group_list);
				
				$next_page_token = $directory_group->getNextPageToken();
				
				if(isset($next_page_token) && !empty($next_page_token)) {
					$_option["pageToken"] = $next_page_token;
					$this->getListGoogleGroups($_option);
				} else {
					return;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get google group list; getListGoogleGroups(); ERROR[" . $e->getMessage() . "]");
				return;
			}
		}
		
		function insertGroupFromGoogle() {
			try {
				$this->getListGoogleGroups();
				$google_group_list = $this->getGoogleGroupList();
					
				$this->mysql->rawQuery("UPDATE google_groups SET yn_enable = ?", array("N"));
					
				if(count($google_group_list) > 0) {
					foreach($google_group_list as $group) {
						$data = Array ($group->getId(),
								$group->getName(),
								$group->getEmail(),
								$group->getDescription(),
								$group->getDirectMembersCount(),
								$group->getAdminCreated() ? "Y" : "N",
								"N",
								"Y"
								);
				
						$this->mysql->rawQuery("INSERT INTO google_groups(google_groups_id, group_name, group_email, description, direct_members_count, yn_admin_created, yn_default_group, yn_enable)
							VALUES(?, ?, ?, ?, ?, ?, ?, ?)
							ON DUPLICATE KEY UPDATE group_name = VALUES(group_name), group_email = VALUES(group_email), description = VALUES(description), direct_members_count = VALUES(direct_members_count),
													yn_admin_created = VALUES(yn_admin_created), yn_enable = VALUES(yn_enable)", $data);
					}
				}
					
				$this->mysql->rawQuery("DELETE FROM google_groups WHERE yn_enable = ?", array("N"));
			} catch(Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert group from google; insertGroupFromGoogle(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function createGroup($_group_name, $_group_email, $_description, $_store_name, $_store_address, $_store_phone, $_store_manager, $_store_parent_id, $_internet_provider, $_phone_provider) {
			try {
				$new_group = new Google_Service_Directory_Group();
				$new_group->setId($_group_email);
				$new_group->setName($_group_name);
				$new_group->setEmail($_group_email);
				$new_group->setDescription($_description);
					
				$ret_group = $this->directory_service->groups->insert($new_group);
				$id = null;
				if(isset($_store_name) && !empty($_store_name)) {
					$data = Array (
							"store_name" => $_store_name,
							"store_address" => $_store_address,
							"store_phone" => $_store_phone,
							"manager_user_id" => $_store_manager,
							"google_groups_id" => $ret_group->getId(),
							"parent_id" => $_store_parent_id,
							"internet_provider" => $_internet_provider,
							"phone_provider" => $_phone_provider
							);
						
					$id = $this->mysql->insert('store', $data);
					if(!$id) {
						throw new Exception($this->mysql->getLastError());
					}
				}
				
				$ret = array(
					"store_pk" => $id,
					"store_name" => $_store_name,
					"text" => $_store_name,
					"store_address" => $_store_address,
					"store_phone" => $_store_phone,
					"manager_user_id" => $_store_manager,
					"parent_id" => $_store_parent_id,
					"internet_provider" => $_internet_provider,
					"phone_provider" => $_phone_provider,
					"google_groups_id" => $ret_group->getId(),
					"group_name" => $ret_group->getName(),
					"group_email" => $ret_group->getEmail(),
					"description" => $ret_group->getDescription(),
					"direct_members_count" => $ret_group->getDirectMembersCount() == null ? 0 : $ret_group->getDirectMembersCount()
				);
				
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to create new group; createGroup(); ERROR[" . $e->getMessage() . "]");
				return null;
			}
		}
		
		function updateGroup($_google_groups_id, $_group_name, $_group_email, $_description, $_store_name, $_store_address, $_store_phone, $_store_manager, $_store_parent_id, $_store_pk, $_internet_provider, $_phone_provider) {
			try {
				$new_group = new Google_Service_Directory_Group();
				$new_group->setName($_group_name);
				$new_group->setEmail($_group_email);
				$new_group->setDescription($_description);
				
				$ret_group = $this->directory_service->groups->update($_google_groups_id, $new_group);
				
				$new_store_pk = null;
				if(isset($_store_name) && !empty($_store_name)) {
					$data = Array (
						"store_name" => $_store_name,
						"store_address" => $_store_address,
						"store_phone" => $_store_phone,
						"manager_user_id" => $_store_manager,
						"parent_id" => $_store_parent_id,
						"google_groups_id" => $_google_groups_id,
						"internet_provider" => $_internet_provider,
						"phone_provider" => $_phone_provider
					);
					
					if(isset($_store_pk) && !empty($_store_pk)) {
						$this->mysql->where("store_pk", $_store_pk);
						$retUpdate = $this->mysql->update('store', $data);
						
						if($retUpdate != 1) {
							throw new Exception($this->mysql->getLastError());
						}
						
						$new_store_pk = $_store_pk;
					} else {
						$new_store_pk = $this->mysql->insert('store', $data);
						if(!$new_store_pk) {
							throw new Exception($this->mysql->getLastError());
						}
					}
				}
				
				$ret = array(
					"store_pk" => $new_store_pk,
					"store_name" => $_store_name,
					"text" => $_store_name,
					"store_address" => $_store_address,
					"store_phone" => $_store_phone,
					"manager_user_id" => $_store_manager,
					"parent_id" => $_store_parent_id,
					"internet_provider" => $_internet_provider,
					"phone_provider" => $_phone_provider,
					"google_groups_id" => $ret_group->getId(),
					"group_name" => $ret_group->getName(),
					"group_email" => $ret_group->getEmail(),
					"description" => $ret_group->getDescription(),
					"direct_members_count" => $ret_group->getDirectMembersCount() == null ? 0 : $ret_group->getDirectMembersCount()
				);
		
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update group; updateGroup(); ERROR[" . $e->getMessage() . "]");
				return null;
			}
		}
		
		function deleteGroup($_google_groups_id) {
			try {
				$ret_group = $this->directory_service->groups->delete($_google_groups_id);
				$this->mysql->where("google_groups_id", $_google_groups_id);
				$this->mysql->delete("store");
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to delete google group; deleteGroup(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function changeGroupToDefault($_group_id, $_yn_default) {
			try {
				$this->mysql->rawQuery("UPDATE google_groups SET yn_default_group = ? WHERE google_groups_id = ?", array($_yn_default, $_group_id));
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to change group to default; changeGroupToDefault(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function getLocalStoreList() {
			return $this->local_store_list;
		}
		
		function getLocalStoreListAsJson() {
			try {
				if(count($this->local_store_list) > 0) {
					if(!$this->is_local_store_json_format) {
						$this->makeLocalStoreListAsJsonFormat();
					}
		
					return json_encode($this->local_store_list);
				} else {
					return "[]";
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get local store list as json; getLocalStoreListAsJson(); ERROR[" . $e->getMessage() . "]");
				return "[]";
			}
		}
		
		function makeLocalStoreListAsJsonFormat() {
			try {
				$store_group_list = array();
				foreach($this->local_store_list as $row) {
					$store_group_list[$row["google_groups_id"]] = $row;
				}
									
				$this->local_store_list = $store_group_list;
				$this->is_local_store_json_format = true;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to make local store list as json format; makeLocalStoreListAsJsonFormat(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function getListLocalStores() {
			try {
				$this->local_store_list = array();
				$local_store = $this->mysql->rawQuery("SELECT store_pk, store_name, store_name AS text, store_address, store_phone, manager_user_id, parent_id, google_groups_id, internet_provider, phone_provider FROM store ORDER BY store_name;");
// 				$local_store = $this->mysql->rawQuery("SELECT s.store_pk, s.store_name, s.store_name AS text, s.store_address, s.store_phone, s.manager_user_id, s.parent_id, s.google_groups_id, g.group_name, g.group_email, g.description, g.direct_members_count
// 														FROM store s LEFT JOIN google_groups g ON g.google_groups_id = s.google_groups_id 
// 														ORDER BY s.store_name;");
					
				if($this->mysql->count <= 0) {
					return;
				}
					
				foreach ($local_store as $group) {
					if(!isset($this->local_store_list[$group["google_groups_id"]])) {
						$this->local_store_list[$group["google_groups_id"]] = array();
					}
		
					$this->local_store_list[$group["google_groups_id"]] = $group;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get local group list; getListLocalGroups(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function getTotalGroupStoreList() {
			try {
				$this->getListGoogleGroups();
				$google_group_list = $this->getGoogleGroupList();
				
				$this->getListLocalStores();
				$local_store_list = $this->getLocalStoreList();
				
				$store_list = array();
				foreach ($google_group_list as $google_group_row) {
					$store_list[$google_group_row->getId()] = array();
					$store_list[$google_group_row->getId()]["google_groups_id"] = $google_group_row->getId();
					$store_list[$google_group_row->getId()]["group_name"] = $google_group_row->getName();
					$store_list[$google_group_row->getId()]["group_email"] = $google_group_row->getEmail();
					$store_list[$google_group_row->getId()]["description"] = $google_group_row->getDescription();
					$store_list[$google_group_row->getId()]["direct_members_count"] = $google_group_row->getDirectMembersCount() == null ? 0 : $google_group_row->getDirectMembersCount();
					$store_list[$google_group_row->getId()]["store_pk"] = -1;
					$store_list[$google_group_row->getId()]["store_name"] = "";
					$store_list[$google_group_row->getId()]["text"] = "";
					$store_list[$google_group_row->getId()]["store_address"] = "";
					$store_list[$google_group_row->getId()]["store_phone"] = "";
					$store_list[$google_group_row->getId()]["manager_user_id"] = "";
					$store_list[$google_group_row->getId()]["parent_id"] = -1;
					$store_list[$google_group_row->getId()]["internet_provider"] = "";
					$store_list[$google_group_row->getId()]["phone_provider"] = "";
				}
				
				foreach ($local_store_list as $k => $local_store_row) {
					if(isset($store_list[$k])) {
						$store_list[$k]["store_pk"] = $local_store_row["store_pk"];
						$store_list[$k]["store_name"] = $local_store_row["store_name"];
						$store_list[$k]["text"] = $local_store_row["text"];
						$store_list[$k]["store_address"] = $local_store_row["store_address"];
						$store_list[$k]["store_phone"] = $local_store_row["store_phone"];
						$store_list[$k]["manager_user_id"] = $local_store_row["manager_user_id"];
						$store_list[$k]["parent_id"] = $local_store_row["parent_id"];
						$store_list[$k]["internet_provider"] = $local_store_row["internet_provider"];
						$store_list[$k]["phone_provider"] = $local_store_row["phone_provider"];
					}
				}
				
				return $store_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get local group list; getListLocalGroups(); ERROR[" . $e->getMessage() . "]");
				return array();
			}
		}
	}
?>