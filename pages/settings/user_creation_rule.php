<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../../inc/config.php");
		require_once_autoload();
		require_once_classes(array("CSession", "CGoogleClientManager", "CMysqlManager", "CGroupManager"));
	
		$session = new CSession();
	
		if($session->isLogin == false) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$google_client = new CGoogleClientManager($session->getAccessToken());
		if($google_client->isAccessTokenExpired()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$mysql_manager = new CMysqlManager();
		$group_manager = new CGroupManager($google_client->getGoogleClient(), $mysql_manager->getDb());
		$group_manager->getListLocalGroups();
		$local_group_list = $group_manager->getLocalGroupListAsJson();
	} catch (Exception $e) {
		qbw_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > Settings > User Creation Rule"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-select.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-toggle.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/moment.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/transition.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/collapse.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-toggle.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-multiselect.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/underscore-min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/settings.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var list_table = null;
			var local_group_list = <?php echo $local_group_list; ?>;
			
			$(function() {
				list_table = $('#group_list').DataTable({
					columns: [
						null,
						null,
						null,
						null,
						{ "visible": false },
						{ "orderData": 4}
					],
					drawCallback : function() {
						$('.btnYnDefaultToggle').bootstrapToggle({
							on: 'Default',
							off: 'None',
							width: '100px'
						}).off("change").on("change", function() {
							var yn_default = $(this).is(':checked') ? "Y" : "N";
							var id = $(this).parent("div").parent("td").parent("tr").attr("id");

							changeDefaultStatus(id, yn_default);
						});
					}
				});

				$.each(local_group_list, function(key, val) {
					var ynDefaultHtml = "<input type='checkbox' class='btnYnDefaultToggle' " + (val.yn_default_group == "Y" ? "checked" : "") + "/>";
							
					var new_node = list_table.row.add([
						val.group_name,
						val.group_email,
						val.description,
						val.direct_members_count,
						val.yn_default_group == "Y" ? "default" : "none",
						ynDefaultHtml
					]).node();

					$(new_node).attr("id", val.google_groups_id);
				});

				list_table.draw();
			});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Settings > User Creation Rule</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">Group List</div>
							<div class="panel-body">
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="group_list">
										<thead>
											<tr>
												<th>Group Name</th>
												<th>Group Email</th>
												<th>Descritpion</th>
												<th>Members Count</th>
												<th>Default Group</th>
												<th>Default Group</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>