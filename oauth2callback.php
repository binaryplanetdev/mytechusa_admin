<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("inc/config.php");
		require_once_autoload();
		require_once_classes(array("CSession", "CGoogleClientManager"));
		
		$google_client = new CGoogleClientManager();
		
		if (!isset($_GET ['code'])) {
			$auth_url = $google_client->createAuthUrl();
			
			moveToSpecificPage($auth_url);
		} else {
			$session = new CSession();
			$session->logout();
			
			$google_client->authenticate($_GET['code']);
			
			$ret = $google_client->checkPermission();
			if($ret == true) {
				$session->login($google_client->getAccessToken());
				moveToSpecificPage(CONF_URL_INDEX);
			} else {
				moveToSpecificPage(CONF_URL_LOGOUT, CONF_URL_ERROR);
			}
		}
	} catch (Exception $e) {
		qbw_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>