<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../../inc/config.php");
		require_once_autoload();
		require_once_classes(array("CSession", "CGoogleClientManager", "CMysqlManager", "CUserManager", "CGroupManager", "CMemberManager"));
	
		$session = new CSession();
	
		if($session->isLogin == false) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$google_client = new CGoogleClientManager($session->getAccessToken());
		if($google_client->isAccessTokenExpired()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$mysql_manager = new CMysqlManager();
		
		$user_manager = new CUserManager($google_client->getGoogleClient(), $mysql_manager->getDb());
		$user_manager->getListGoogleUsers();
		$google_user_list_json = $user_manager->getGoogleUserListAsJson();
				
		$group_manager = new CGroupManager($google_client->getGoogleClient(), $mysql_manager->getDb());
		$group_manager->getListLocalGroups();
		$local_groups_json = $group_manager->getLocalGroupListAsJson();
		
		$group_manager->getListGoogleGroups();
		$google_group_json = $group_manager->getGoogleGroupListAsJson();
		
		$member_manager = new CMemberManager($google_client->getGoogleClient(), $mysql_manager->getDb());
		$member_manager->getListLocalMembers();
		$local_member_json = $member_manager->getLocalMemberListAsJson();
	} catch (Exception $e) {
		qbw_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > Staff Manager > Staff"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-select.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-toggle.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/moment.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/transition.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/collapse.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-toggle.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-multiselect.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/underscore-min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/staff.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var login_url = "<?php echo CONF_URL_LOGIN; ?>";
			var host_domain = "<?php echo HOST_DOMAIN; ?>";
			var list_table = null;
			var google_user_list = <?php echo $google_user_list_json; ?>;
			var local_groups = <?php echo $local_groups_json;?>;
			var group_list = <?php echo $google_group_json;?>;
			var local_members = <?php echo $local_member_json;?>;

			var emailForwardingHtml = "<button type='button' class='btn btn-primary btnForwardMail' data-toggle='modal' data-target='#modal_popup'>Email Forwarding</button>";
			var deleteStaffHtml = "<button type='button' class='btn btn-danger btnDeleteStaff'>Delete</button>";

			$(function() {
				list_table = $('#user_list').DataTable({
					columns: [
						null,
						null,
						null,
						null,
						{ "visible": false },
						{ "orderData": 4},
						{ "orderable": false },
						{ "orderable": false }
					],
					initComplete: function () {
						var html = "";
						html += "<label>";
						html += "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modal_popup' id='btnCreateNewUser'>Create User</button>";
						html += "</label>";
						$('#user_list_length').html(html);
						
						$('#btnCreateNewUser').off("click").on('click', function() {
							showUserPopup();
						});
					},
					drawCallback: function() {						
						$('#user_list > tbody').off("click").on('click', 'tr', function (event) {
							var index = $(this).index("#user_list > tbody > tr");
							if($(event.target).is('#user_list > tbody > tr:eq(' + index + ') > td:eq(0),#user_list > tbody > tr:eq(' + index + ') > td:eq(1),#user_list > tbody > tr:eq(' + index + ') > td:eq(2),#user_list > tbody > tr:eq(' + index + ') > td:eq(3)')) {
								var id = $(this).attr("id");
						        var user_data = google_user_list[id];
						        
						        $('#modal_popup').modal('show');

						        showUserPopup(user_data);
							}
						});
						
						$('.btnYnSuspendedToggle').bootstrapToggle({
							on: 'Active',
							off: 'Suspend'
						}).off("change").on("change", function() {
							var yn_suspended = $(this).is(':checked') ? "N" : "Y";
							var id = $(this).parent("div").parent("td").parent("tr").attr("id");

							changeUserYnSuspended(id, yn_suspended);
						});

						$('.btnForwardMail').off("click").on("click", function() {
							var user_id = $(this).parent("td").parent("tr").attr("id");
							var user_data = google_user_list[user_id];
							
							showEmailForwardingPopup(user_data);
						});

						$('.btnDeleteStaff').off("click").on("click", function() {
							var result = confirm("Are you sure?");
							if(result) {
								var user_id = $(this).parent("td").parent("tr").attr("id");
								deleteStaff(user_id);
							}
						});
					}
				});
				
				$.each(google_user_list, function(key, val) {
					var ynSuspendedHtml = "<input type='checkbox' class='btnYnSuspendedToggle' " + (val.yn_suspended == "Y" ? "" : "checked") + " />";
					
					var new_node = list_table.row.add([
						val.full_name,
						val.primary_email,
						val.phone,
						val.address,
						val.yn_suspended == "Y" ? "Suspended" : "Active",
						ynSuspendedHtml,
						emailForwardingHtml,
						deleteStaffHtml
					]).node();

					$(new_node).attr("id", val.user_id);
				});

				list_table.draw();
			});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Staff Manager > Staff</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">Staff List</div>
							<div class="panel-body">
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="user_list">
										<thead>
											<tr>
												<th>Name</th>
												<th>Email</th>
												<th>Phone</th>
												<th>Address</th>
												<th>Status</th>
												<th>Status</th>
												<th>Email Forwarding</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="modal_popup_content"></div>
						<div class="modal-footer" id="modal_popup_footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>