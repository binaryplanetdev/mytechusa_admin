<?php
	session_start();

	/**
	 * DATABASE
	 */
	define("DEF_MYSQL_IP",		"localhost");
	define("DEF_MYSQL_PORT",	3306);
	define("DEF_MYSQL_USER",	"root");
	define("DEF_MYSQL_PASS",	"");
	define("DEF_MYSQL_DB",		"mytechgoogle");
	
	/**
	 * PATH
	 */
	define('CONF_SERVER_HOST',		"http://" . $_SERVER['HTTP_HOST']);
	
	define('CONF_URL_ROOT',			"/GoogleApi/");
	define('CONF_PATH_ASSETS', 		CONF_URL_ROOT . "assets/");
	
	define("CONF_PATH_ROOT",		$_SERVER["DOCUMENT_ROOT"] . CONF_URL_ROOT);
	define("CONF_PATH_CLASS",		CONF_PATH_ROOT . "inc/classes/");
	define('CONF_PATH_DEBUG_FILE',	CONF_PATH_ROOT . "logs/");
	define('CONF_PATH_DATA',		CONF_PATH_ROOT . "data/");
	
	/**
	 * DEBUG
	 */
	define('CONF_DEBUG',			true);
	define('CONF_DEBUG_INFO',		false);
	define('CONF_DEBUG_ERROR',		true);
	define('CONF_DEBUG_DISPLAY',	false);
	define('CONF_DEBUG_SAVE',		true);
	define('CONF_DEBUG_SAVE_FILE_ERROR',	'bc_debug_mesg_error.log');
	define('CONF_DEBUG_SAVE_FILE_ETC',		'bc_debug_mesg_etc.log');
	
	define("CONF_SITE_TITLE", "MyTech USA Admin");
	
	/**
	 * HOST DOMAIN
	 */
	define('HOST_DOMAIN',	"binaryplanet.brianyu.ca");
	
	/**
	 * DEFINE
	 */
	include_once('config_master.php');
	
	/**
	 * GLOBAL FUNCTIONS
	 */
	include_once("global_function.php");
?>