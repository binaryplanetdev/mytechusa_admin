<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../../inc/config.php");
		require_once_autoload();
		require_once_classes(array("CSession", "CGoogleClientManager", "CMysqlManager", "CUserManager", "CGroupManager"));
	
		$session = new CSession();
	
		if($session->isLogin == false) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$google_client = new CGoogleClientManager($session->getAccessToken());
		if($google_client->isAccessTokenExpired()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$mysql_manager = new CMysqlManager();
		$user_manager = new CUserManager($google_client->getGoogleClient(), $mysql_manager->getDb());
		$group_manager = new CGroupManager($google_client->getGoogleClient(), $mysql_manager->getDb());
		
		$user_manager->getListGoogleUsers();
		$google_user_json = $user_manager->getGoogleUserListAsJson();
		
		$store_list = $group_manager->getTotalGroupStoreList();
		$store_list_json = json_encode($store_list);
		
		$group_manager->getListLocalStores();
		$local_store_list = $group_manager->getLocalStoreList();
		
		$tree = buildTree($local_store_list);
		$store_tree = json_encode($tree);
	} catch (Exception $e) {
		qbw_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > Store Manager > Store"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-select.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-treeview.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/moment.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/transition.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/collapse.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-multiselect.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-treeview.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/group.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
		var host_domain = "<?php echo HOST_DOMAIN; ?>";
		var list_table = null;
		var google_user_list = <?php echo $google_user_json; ?>;
		var tree = <?php echo $store_tree; ?>;
		var store_list = <?php echo $store_list_json; ?>;
		var selectedParent = null;
		var deleteGroupHtml = "<button type='button' class='btn btn-danger btnDeleteGroup'>Delete</button>";
		var page = "store";

		$(function() {
			list_table = $('#group_list').DataTable({
				columns: [
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					{ "orderable": false }
				],
				initComplete: function () {
					var html = "";
					html += "<label>";
					html += "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modal_popup' id='btnCreateNewStore'>Create Store</button>";
					html += "</label>";
					$('#group_list_length').html(html);
					
					$('#btnCreateNewStore').off("click").on('click', function() {
						showGroupPopup();
					});
				},
				drawCallback : function() {
					$('#group_list > tbody').off('click').on('click', 'tr', function () {
						var google_groups_id = $(this).attr("id");
				        var group_data = store_list[google_groups_id];

				        $('#modal_popup').modal('show');

				        showGroupPopup(group_data);
					});

					$('.btnDeleteGroup').off("click").on("click", function() {
						var result = confirm("Are you sure?");
						if(result) {
							var google_groups_id = $(this).parent("td").parent("tr").attr("id");
							deleteGroup(google_groups_id);
						}
						
						return false;
					});
				}
			});

			$.each(store_list, function(key, val) {
				if(val.store_name.length > 0) {					
					var new_node = list_table.row.add([
						val.store_name,
						val.store_phone,
						val.store_address,
						google_user_list[val.manager_user_id] != null ? google_user_list[val.manager_user_id].full_name : "",
						val.internet_provider,
						val.phone_provider,
						val.group_name,
						val.group_email,
						deleteGroupHtml
					]).node();
	
					$(new_node).attr("id", val.google_groups_id);
				}
			});

			list_table.draw();
		});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Store Manager > Store</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">Store List</div>
							<div class="panel-body">
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="group_list">
										<thead>
											<tr>
												<th>Store Name</th>
												<th>Store Phone</th>
												<th>Store Location</th>
												<th>Store Manager</th>
												<th>Internet Provider</th>
												<th>Phone Provider</th>
												<th>Group Name</th>
												<th>Group Email</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="modal_popup_content"></div>
						<div class="modal-footer" id="modal_popup_footer"></div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="tree_modal_popup" tabindex="-1" role="dialog" aria-labelledby="tree_modal_popup_label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="tree_modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="tree_modal_popup_content"></div>
						<div class="modal-footer" id="tree_modal_popup_footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>