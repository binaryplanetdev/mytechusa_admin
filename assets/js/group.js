function showGroupPopup(_group_data) {
	var mode = _group_data == null ? "create" : "edit";
	
	console.log(_group_data);
	selectedParent = null;
	var email;
	if(mode == "edit") {
		email = _group_data.group_email.split("@");
		
		if(_group_data.parent_id > 0) {
			$.each(store_list, function(key, data) {
				if(data.store_pk == _group_data.parent_id) {
					selectedParent = data;
					return;
				}
			});
		}
	}
	
	var html = "";
	html += "<div class='row'>";
	html += "<ul class='nav nav-tabs'>";
	html += "<li class='active'><a href='#group_info_tab' data-toggle='tab'>Group Info</a></li>";
	html += "<li><a href='#store_info_tab' data-toggle='tab'>Store Info</a></li>";
	html += "</ul>";
	html += "<div class='tab-content' style='padding-top: 15px;'>";
	html += "<div class='tab-pane fade in active' id='group_info_tab'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='group_name'>";
	html += "<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Group Name";
	html += "</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='group_name' value='" + (mode == "edit" ? _group_data.group_name : "") + "'/>";
	html += "<p class='help-block'>Name of the group</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='group_email'>";
	html += "<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Group Email";
	html += "</label>";
	html += "<div class='col-sm-5' style='padding-right: 0px;'>";
	html += "<input type='text' class='form-control' id='group_email' value='" + (mode == "edit" ? email[0] : "") + "'/>";
	html += "<p class='help-block'>Group email address</p>";
	html += "</div>";
	html += "<div class='col-sm-4' style='padding-left: 3px;'>";
	html += "<span>@" + host_domain + "</span>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='description'>Descritpion</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='description' value='" + (mode == "edit" ? _group_data.description : "") + "'/>";
	html += "<p class='help-block'>Description(optional)</p>";
	html += "</div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	html += "<div class='tab-pane fade' id='store_info_tab'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='store_name'>Store Name</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='store_name' value='" + (mode == "edit" ? _group_data.store_name : "") + "'/>";
	html += "<p class='help-block'>Name of the store</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='store_address'>Store Address</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='store_address' value='" + (mode == "edit" ? _group_data.store_address : "") + "'/>";
	html += "<p class='help-block'>Store address</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='store_phone'>Store Phone</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='store_phone' value='" + (mode == "edit" ? _group_data.store_phone : "") + "'/>";
	html += "<p class='help-block'>Store phone</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='internet_provider'>Internet Provider</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='internet_provider' value='" + (mode == "edit" ? _group_data.internet_provider : "") + "'/>";
	html += "<p class='help-block'>Store Internet Provider Information</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='phone_provider'>Phone Provider</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='phone_provider' value='" + (mode == "edit" ? _group_data.phone_provider : "") + "'/>";
	html += "<p class='help-block'>Store Phone Provider Information</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='store_manager'>Store Manager</label>";
	html += "<div class='col-sm-9'>";
	html += "<select id='store_manager'></select>";
	html += "<p class='help-block'>Select Store Manager</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='store_parent'>Store Parent</label>";
	html += "<div class='col-sm-5' style='padding-right: 0px;'>";
	html += "<input type='text' class='form-control' id='store_parent' value='" + (mode == "edit" && selectedParent != null ? selectedParent.store_name : "") + "' readonly/>";
	html += "<p class='help-block'>Select store parent</p>";
	html += "</div>";
	html += "<div class='col-sm-4' style='padding-left: 3px;'>";
	html += "<button type='button' class='btn btn-primary' id='btnSelectStoreParent'>Select Parent</button>";
	html += "</div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	html += "</div>";
	html += "</div>";
	
	$('#modal_popup_content').html(html);
	
	var footer_html = "";
	footer_html += "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>";
	
	if(mode == "edit") {
		if(page == "group") {
			$('#modal_popup_label').text("Edit Group");	
		} else {
			$('#modal_popup_label').text("Edit Store");
		}
		
		footer_html += "<button type='button' class='btn btn-primary' id='btnSaveChanges'>Save Changes</button>";		
	} else {
		if(page == "group") {
			$('#modal_popup_label').text("Add New Group");	
		} else {
			$('#modal_popup_label').text("Add New Store");
		}
		
		footer_html += "<button type='button' class='btn btn-primary' id='btnCreateGroup'>Create</button>";
	}
	
	$('#modal_popup_footer').html(footer_html);
	
	if(mode == "edit") {
		setUsers(_group_data.manager_user_id);
		
		$('#btnSaveChanges').off("click").on("click", function() {
			editGroup(_group_data);
		});
	} else {
		setUsers();
		
		$('#btnCreateGroup').off("click").on("click", function() {
			createNewGroup();
		});
	}
	
	$('#store_manager').multiselect({
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		maxHeight: 400,
		buttonWidth: '350px',
		nonSelectedText: 'Select Manager',
		disableIfEmpty: true
	});
	
	$('#btnSelectStoreParent').off("click").on("click", function(){
		$('#modal_popup').modal('hide');
		showStoreParentPopup();
	});
}

function showStoreParentPopup() {
	$('#tree_modal_popup_content').html("<div id='tree'></div>");
	$('#tree_modal_popup_label').text("Select Store Parent");
	
	var footer_html = "";
	footer_html += "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>";
	footer_html += "<button type='button' class='btn btn-primary' id='btnApplyParent'>Apply</button>";
	$('#tree_modal_popup_footer').html(footer_html);
	
	$('#tree').treeview({
		data: tree,
		onNodeSelected: function(event, data) {
			selectedParent = data;
		},
		onNodeUnselected: function(event, data) {
			selectedParent = null;
		}
	});
	
	$('#tree_modal_popup').modal('show');
	
	$('#btnApplyParent').off("click").on("click", function() {
		if(selectedParent == null) {
			alert("Select parent store");
			return;
		}
		
		$('#store_parent').val(selectedParent.store_name);
		$('#tree_modal_popup').modal('hide');
		$('#modal_popup').modal('show');
	});	
}

function setUsers(_manager_user_id) {
	var html = "<option value='0'>Select Manager</option>";
	
	$.each(google_user_list, function(key, data) {
		if(_manager_user_id && _manager_user_id == key) {
			html += "<option value='" + key + "' selected>" + data.full_name + "</option>";
		} else {
			html += "<option value='" + key + "'>" + data.full_name + "</option>";
		}
	});
	
	$('#store_manager').html(html);
}

function createNewGroup() {
	var group_name = $.trim($('#group_name').val());
	var group_email = $.trim($('#group_email').val());
	var description = $.trim($('#description').val());
	var store_name = $.trim($('#store_name').val());
	var store_address = $.trim($('#store_address').val());
	var store_phone = $.trim($('#store_phone').val());
	var store_manager = $.trim($('#store_manager').val());
	var internet_provider = $.trim($('#internet_provider').val());
	var phone_provider = $.trim($('#phone_provider').val());
	var store_parent_id = null;
	if(selectedParent) {
		store_parent_id = selectedParent.store_pk;		
	}
	
	if(group_name.length <= 0) {
		alert("Group name is empty");
		return false;
	}
	
	if(group_email.length <= 0) {
		alert("Group email is empty");
		return false;
	}
	
	if(store_name.length > 0 && store_parent_id == null) {
		alert("Select store parent");
		return false;
	}
	
	group_email += "@" + host_domain;
	
	var params = {};
	params.type = "create_group";
	params.group_name = group_name;
	params.group_email = group_email;
	params.description = description;
	params.store_name = store_name;
	params.store_address = store_address;
	params.store_phone = store_phone;
	params.store_manager = store_manager;
	params.store_parent_id = store_parent_id;
	params.phone_provider = phone_provider;
	params.internet_provider = internet_provider;
	
	$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
		var ret_data = JSON.parse(_data);
		
		if(ret_data.result == "OK") {
			store_list[ret_data.data.google_groups_id] = ret_data.data;			
			
			var new_node;
			if(page == "group") {
				new_node = list_table.row.add([
       				ret_data.data.group_name, 
       				ret_data.data.group_email, 
       				ret_data.data.description,
       				ret_data.data.store_name,
       				ret_data.data.direct_members_count, 
       				deleteGroupHtml
       			]).draw().node();	
			} else {
				new_node = list_table.row.add([
					ret_data.data.store_name,
					ret_data.data.store_phone,
					ret_data.data.store_address,
					google_user_list[val.manager_user_id] != null ? google_user_list[val.manager_user_id].full_name : "",
					ret_data.data.internet_provider,
					ret_data.data.phone_provider,
       				ret_data.data.group_name, 
       				ret_data.data.group_email, 
       				deleteGroupHtml
       			]).draw().node();
			}
			
			
			$(new_node).attr("id", ret_data.data.google_groups_id);
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = login_url;
		} else {
			alert(ret_data.message);
		}
		
		$('#modal_popup').modal('hide');
	}, "text");
}

function editGroup(_group_data) {
	var group_name = $.trim($('#group_name').val());
	var group_email = $.trim($('#group_email').val());
	var description = $.trim($('#description').val());
	var store_name = $.trim($('#store_name').val());
	var store_address = $.trim($('#store_address').val());
	var store_phone = $.trim($('#store_phone').val());
	var store_manager = $.trim($('#store_manager').val());
	var internet_provider = $.trim($('#internet_provider').val());
	var phone_provider = $.trim($('#phone_provider').val());
	var store_parent_id = null;
	
	if(selectedParent) {
		store_parent_id = selectedParent.store_pk;
	}
	
	if(group_name.length <= 0) {
		alert("Group name is empty");
		return false;
	}
	
	if(group_email.length <= 0) {
		alert("Group email is empty");
		return false;
	}
	
	if(store_name.length > 0 && _group_data.parent_id == -1 && store_parent_id == null) {
		alert("Select store parent");
		return false;
	}
	
	var params = {};
	params.type = "edit_group";
	params.google_groups_id = _group_data.google_groups_id;
	params.group_name = group_name;
	params.group_email = group_email;
	params.description = description;
	params.store_pk = _group_data.store_pk;
	params.store_name = store_name;
	params.store_address = store_address;
	params.store_phone = store_phone;
	params.store_manager = store_manager;
	params.store_parent_id = store_parent_id;
	params.internet_provider = internet_provider;
	params.phone_provider = phone_provider;
	
	$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
		var ret_data = JSON.parse(_data);
		console.log(ret_data);
		
		if(ret_data.result == "OK") {
			store_list[ret_data.data.google_groups_id] = ret_data.data;
			
			if(page == "group") {
				list_table.row($('#' + ret_data.data.google_groups_id)).data([
	  				ret_data.data.group_name, 
	  				ret_data.data.group_email,
	  				ret_data.data.description,
	  				ret_data.data.store_name,
	  				ret_data.data.direct_members_count, 
	  				deleteGroupHtml
	  			]).draw();	
			} else {
				list_table.row($('#' + ret_data.data.google_groups_id)).data([
					ret_data.data.store_name,
					ret_data.data.store_phone,
					ret_data.data.store_address,
					google_user_list[val.manager_user_id] != null ? google_user_list[val.manager_user_id].full_name : "",
					ret_data.data.internet_provider,
					ret_data.data.phone_provider,
					ret_data.data.group_name,
					ret_data.data.group_email,
					deleteGroupHtml
      			]).draw();
			}
			
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = login_url;
		} else {
			alert(ret_data.message);
		}
		
		$('#modal_popup').modal('hide');
	}, "text");
}

function deleteGroup(_google_groups_id) {
	var params = {};
	params.type = "delete_group";
	params.google_groups_id = _google_groups_id;
	$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
		var ret_data = JSON.parse(_data);
		
		if(ret_data.result == "OK") {
			list_table.row($('#' + _google_groups_id)).remove().draw();
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = login_url;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}
