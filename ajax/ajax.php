<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_autoload();
		require_once_classes(array("CSession", "CGoogleClientManager", "CMysqlManager", "CUserManager", "CGroupManager", "CMemberManager"));
	
		$session = new CSession();
	
		if($session->isLogin == false) {
			echo bc_return("NOT_LOGIN", "Not Login");
			exit;
		}
	
		$google_client = new CGoogleClientManager($session->getAccessToken());
		if($google_client->isAccessTokenExpired()) {
			echo bc_return("NOT_LOGIN", "Not Login");
			exit;
		}
	
		$mysql_manager = new CMysqlManager();
		$user_manager = new CUserManager($google_client->getGoogleClient(), $mysql_manager->getDb());
		$group_manager = new CGroupManager($google_client->getGoogleClient(), $mysql_manager->getDb());
		$member_manager = new CMemberManager($google_client->getGoogleClient(), $mysql_manager->getDb());
		
		$type = $_POST['type'];
		$ret = array("result" => "OK", "message" => "SUCCESS", "data" => null);
		
		if($type == "sync_with_google") {
			$user_manager->insertUserFromGoogle();
			$group_manager->insertGroupFromGoogle();
			$member_manager->insertMemberFromGoogle($group_manager->getGoogleGroupList());
		} else if($type == "create_group") {
			$group_name = $_POST["group_name"];
			$group_email = $_POST["group_email"];
			$description = $_POST["description"];
			$store_name = $_POST["store_name"];
			$store_address = $_POST["store_address"];
			$store_phone = $_POST["store_phone"];
			$store_manager = $_POST["store_manager"];
			$store_parent_id = $_POST["store_parent_id"];
			$internet_provider = $_POST["internet_provider"];
			$phone_provider = $_POST["phone_provider"];
			
			$ret["data"] = $group_manager->createGroup($group_name, $group_email, $description, $store_name, $store_address, $store_phone, $store_manager, $store_parent_id, $internet_provider, $phone_provider);
		} else if($type == "edit_group") {
			$google_groups_id = $_POST["google_groups_id"];
			$group_name = $_POST["group_name"];
			$group_email = $_POST["group_email"];
			$description = $_POST["description"];
			$store_name = $_POST["store_name"];
			$store_address = $_POST["store_address"];
			$store_phone = $_POST["store_phone"];
			$store_manager = $_POST["store_manager"];
			$store_parent_id = $_POST["store_parent_id"];
			$store_pk = $_POST["store_pk"];
			$internet_provider = $_POST["internet_provider"];
			$phone_provider = $_POST["phone_provider"];
			
			$ret["data"] = $group_manager->updateGroup($google_groups_id, $group_name, $group_email, $description, $store_name, $store_address, $store_phone, $store_manager, $store_parent_id, $store_pk, $internet_provider, $phone_provider);
		} else if($type == "delete_group") {
			$google_groups_id = $_POST["google_groups_id"];
			
			$group_manager->deleteGroup($google_groups_id);
		} else if($type == "create_user") {
			$first_name = $_POST["first_name"];
			$last_name = $_POST["last_name"];
			$primary_email = $_POST["primary_email"];
			$password = $_POST["password"];
			
			$secondary_email = $_POST["secondary_email"];
			$secondary_email_type = $_POST["secondary_email_type"];
			
			$phone = $_POST["phone"];
			$phone_type = $_POST["phone_type"];
			
			$address = $_POST["address"];
			$address_type = $_POST["address_type"];
			
			$group_list = $_POST["group_list"];
			
			$ret["data"] = $user_manager->createUser($first_name, $last_name, $primary_email, $password, $secondary_email, $secondary_email_type, $phone, $phone_type, $address, $address_type, $group_list);
		} else if($type == "edit_user") {
			$user_id = $_POST["user_id"];
			$first_name = $_POST["first_name"];
			$last_name = $_POST["last_name"];
			$primary_email = $_POST["primary_email"];
			$password = $_POST["password"];
			
			$secondary_email = $_POST["secondary_email"];
			$secondary_email_type = $_POST["secondary_email_type"];
			
			$phone = $_POST["phone"];
			$phone_type = $_POST["phone_type"];
			
			$address = $_POST["address"];
			$address_type = $_POST["address_type"];
			
			$inserted_group = isset($_POST["inserted_group"]) ? $_POST["inserted_group"] : array();
			$deleted_group = isset($_POST["deleted_group"]) ? $_POST["deleted_group"] : array();
			
			$ret["data"] = $user_manager->updateUser($user_id, $first_name, $last_name, $primary_email, $password, $secondary_email, $secondary_email_type, $phone, $phone_type, $address, $address_type, $inserted_group, $deleted_group);
		} else if($type == "suspend_user") {
			$user_id = $_POST["user_id"];
			$yn_suspended = $_POST["yn_suspended"];
			
			$ret["data"] = $user_manager->suspendUser($user_id, $yn_suspended);
		} else if($type == "delete_user") {
			$user_id = $_POST["user_id"];
			
// 			$ret["data"] = $user_manager->deleteUser($user_id);
		} else if($type == "email_forwarding") {
			$user_id = $_POST["user_id"];
			$primary_email = $_POST["primary_email"];
			$alias_email = $_POST["alias_email"];
			
			$user_manager->emailForwarding($user_id, $primary_email, $alias_email);
		} else if($type == "change_group_default") {
			$group_id = $_POST["group_id"];
			$yn_default = $_POST["yn_default"];
			
			$group_manager->changeGroupToDefault($group_id, $yn_default);
		} else if($type == "staff_data_by_group") {
			$google_groups_id = $_POST["google_groups_id"];
			
			$ret["data"] = $user_manager->getUsersByGroupId($google_groups_id);
		}
	} catch (Exception $e) {
		echo bc_return("ERROR", $e->getMessage());
		exit;
	}
	
	echo bc_return($ret["result"], $ret["message"], $ret["data"]);
	exit;
?>