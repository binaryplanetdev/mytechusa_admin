# 프로젝트 개요 #
* 프로젝트 명 : MyTechUSA Admin site
* 주제 : Google Apps for Work 연동 admin site
* 태그 : Google Apps for Work / Admin / MyTechUSA / Google Directory Api

# 프로젝트 구조 #
* MyTechUSA : PHP 웹 소스 
* 개발 서버 정보
* DB : mysql : host=localhost / id=root / pwd= / db=mytechgoogle
* 테스트 계정 : id=ymcho@binaryplanet.brianyu.ca / pwd=조영민
* 상용 서버 정보
* DB : mysql : host=localhost / id=mytecoxb_google / pwd=mytecoxb_google / db=mytecoxb_google
* 테스트 계정 : id=sunwoo.kim.test@publicelectronics.com / pwd=바이너리플래닛

# 프로젝트 개발 환경 구성을 위한 설치 준비 사항 #
* Google Apps for Work
* Google Api 설정 
* Google OAuth 2.0 client IDs 생성
* Google Directory API Client Library for PHP
* My-SQL
* Web Server / PHP