<?php
	/**
	 * URL
	 */
	define('CONF_URL_INDEX',			CONF_URL_ROOT . "index.php");
	define('CONF_URL_OAUTH_CALLBACK',	CONF_URL_ROOT . "oauth2callback.php");
	
	define('CONF_URL_PAGE_ROOT',	CONF_URL_ROOT . "pages/");
	define('CONF_URL_MENU',			CONF_PATH_ROOT . "pages/menu.php");
	define('CONF_URL_LOGIN',		CONF_URL_PAGE_ROOT . "login.php");
	define('CONF_URL_LOGOUT',		CONF_URL_PAGE_ROOT . "logout.php");
	define('CONF_URL_ERROR',		CONF_URL_PAGE_ROOT . "error.php");
	
	define('CONF_URL_STAFF_ROOT',	CONF_URL_PAGE_ROOT . "staff_manager/");
	define('CONF_URL_STAFF',		CONF_URL_STAFF_ROOT . "staff.php");
	
	define('CONF_URL_STORE_ROOT',		CONF_URL_PAGE_ROOT . "store_manager/");
	define('CONF_URL_GROUP',			CONF_URL_STORE_ROOT . "group.php");
	define('CONF_URL_STORE',			CONF_URL_STORE_ROOT . "store.php");
	define('CONF_URL_STORE_DIRECTORY',	CONF_URL_STORE_ROOT . "store_directory.php");
	
	define('CONF_URL_SETTINGS_ROOT',		CONF_URL_PAGE_ROOT . "settings/");
	define('CONF_URL_SYNC_WITH_GOOGLE',		CONF_URL_SETTINGS_ROOT . "sync_with_google.php");
	define('CONF_URL_USER_CREATION_RULE',	CONF_URL_SETTINGS_ROOT . "user_creation_rule.php");
	
	define('CONF_PATH_GOOGLE_CLIENT_SECRET',	CONF_PATH_DATA . "client_secrets.json");
?>