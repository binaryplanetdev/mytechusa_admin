<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<ul class="nav navbar-top-links navbar-right">
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="<?php echo CONF_URL_LOGOUT . "?redirect=" . CONF_URL_INDEX; ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
			</ul>
		</li>
	</ul>
	<div class="navbar-header"><a class="navbar-brand" href="<?php echo CONF_URL_INDEX; ?>">MyTech USA Admin</a></div>
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<li>
					<a href="<?php echo CONF_URL_INDEX; ?>"><i class="fa fa-dashboard fa-fw"></i>Home</a>
				</li>
				<li>
					<a href="#"><i class="fa fa-group fa-fw"></i>Staff Manager<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="<?php echo CONF_URL_STAFF; ?>">Staff</a></li>
					</ul>
				</li>
				<li>
					<a href="#"><i class="fa fa-sitemap fa-fw"></i>Store Manager<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="<?php echo CONF_URL_GROUP; ?>">Group</a></li>
						<li><a href="<?php echo CONF_URL_STORE; ?>">Store</a></li>
						<li><a href="<?php echo CONF_URL_STORE_DIRECTORY; ?>">Store Directory</a></li>
					</ul>
				</li>
				<li>
					<a href="#"><i class="fa fa-sitemap fa-fw"></i>Settings<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="<?php echo CONF_URL_SYNC_WITH_GOOGLE; ?>">Synchronize with Google</a></li>
						<li><a href="<?php echo CONF_URL_USER_CREATION_RULE; ?>">User Creation Rule</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>