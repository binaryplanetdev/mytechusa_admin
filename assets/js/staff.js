function showUserPopup(_user_data) {
	console.log(login_url);
	var mode = _user_data == null ? "create" : "edit";
	var name;
	var email;
	
	if(mode == "edit") {
		email = _user_data.primary_email.split("@");
	}
	
	var html = "";
	html += "<div class='row'>";
	html += "<ul class='nav nav-tabs'>";
	html += "<li class='active'><a href='#user_info_tab' data-toggle='tab'>User Info</a></li>";
	html += "<li><a href='#group_info_tab' data-toggle='tab'>Group Info</a></li>";
	html += "<li><a href='#additioanl_tab' data-toggle='tab'>Additional Info</a></li>";
	html += "</ul>";
	html += "<div class='tab-content' style='padding-top: 15px;'>";
	html += "<div class='tab-pane fade in active' id='user_info_tab'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='first_name'>";
	html += "<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>First Name";
	html += "</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='first_name' value='" + (mode == "edit" ? _user_data.first_name : "") + "'/>";
	html += "<p class='help-block'>First name</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='last_name'>";
	html += "<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Last Name";
	html += "</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='last_name' value='" + (mode == "edit" ? _user_data.last_name : "") + "'/>";
	html += "<p class='help-block'>Last name</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='primary_email'>";
	html += "<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Primary Email";
	html += "</label>";
	html += "<div class='col-sm-5' style='padding-right: 0px;'>";
	html += "<input type='text' class='form-control' id='primary_email' value='" + (mode == "edit" ? email[0] : "") + "' />";
	html += "<p class='help-block'>Primary email address</p>";
	html += "</div>";
	html += "<div class='col-sm-4' style='padding-left: 3px;'>";
	html += "<span>@" + host_domain + "</span>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='password'>";
	html += "<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Password";
	html += "</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='password' class='form-control' id='password'/>";
	html += "<p class='help-block'>Password</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='password_confirm'>";
	html += "<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Password Confirm";
	html += "</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='password' class='form-control' id='password_confirm'/>";
	html += "<p class='help-block'>Re-enter password</p>";
	html += "</div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	html += "<div class='tab-pane fade' id='group_info_tab'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='group_select'>Group</label>";
	html += "<div class='col-sm-9'>";
	html += "<select id='group_select' multiple='multiple'></select>";
	html += "</div>";
	html += "</div>";
	html += "</form>";
	html += "<div id='added_group_list'>";
	html += "<div class='row'>";
	html += "<div class='col-lg-12'>";
	html += "<div class='panel panel-success'>";
	html += "<div class='panel-heading'>Added Group</div>";
	html += "<div class='panel-body'>";
	html += "<div class='table-responsive'>";
	html += "<table class='table table-striped'>";
	html += "<thead>";
	html += "<tr>";
	html += "<th>Group Name</th>";
	html += "<th>Group Email</th>";
	html += "<th>Description</th>";
	html += "<th class='col-sm-1'>X</th>";
	html += "</tr>";
	html += "</thead>";
	html += "<tbody id='added_group_tbody'></tbody>";
	html += "</table>";
	html += "</div>";
	html += "</div>";
	html += "</div>";
	html += "</div>";
	html += "</div>";
	html += "</div>";
	html += "</div>";
	html += "<div class='tab-pane fade' id='additioanl_tab'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='secondary_email'>Secondary Email</label>";
	html += "<div class='col-sm-5'>";
	html += "<input type='text' class='form-control' id='secondary_email' value='" + (mode == "edit" ? _user_data.secondary_email : "") + "'/>";
	html += "<p class='help-block'>Secondary email address</p>";
	html += "</div>";
	html += "<div class='col-sm-4'>";
	html += "<select class='form-control' id='secondary_email_type'>";
	html += "<option value='home' " + (mode == "edit" && _user_data.secondary_email_type == "home" ? "selected" : "") + ">Home</option>";
	html += "<option value='work' " + (mode == "edit" && _user_data.secondary_email_type == "work" ? "selected" : "") + ">Work</option>";
	html += "<option value='custom' " + (mode == "edit" && _user_data.secondary_email_type == "custom" ? "selected" : "") + ">Custom</option>";
	html += "<option value='other' " + (mode == "edit" && _user_data.secondary_email_type == "other" ? "selected" : "") + ">Other</option>";
	html += "</select>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='phone'>Phone</label>";
	html += "<div class='col-sm-5'>";
	html += "<input type='text' class='form-control' id='phone' value='" + (mode == "edit" ? _user_data.phone : "") + "'/>";
	html += "<p class='help-block'>Phone</p>";
	html += "</div>";
	html += "<div class='col-sm-4'>";
	html += "<select class='form-control' id='phone_type'>";
	html += "<option value='mobile' " + (mode == "edit" && _user_data.phone_type == "mobile" ? "selected" : "") + ">Mobile</option>";
	html += "<option value='home' " + (mode == "edit" && _user_data.phone_type == "home" ? "selected" : "") + ">Home</option>";
	html += "<option value='work' " + (mode == "edit" && _user_data.phone_type == "work" ? "selected" : "") + ">Work</option>";
	html += "<option value='main' " + (mode == "edit" && _user_data.phone_type == "main" ? "selected" : "") + ">Main</option>";
	html += "<option value='custom' " + (mode == "edit" && _user_data.phone_type == "custom" ? "selected" : "") + ">Custom</option>";
	html += "<option value='other' " + (mode == "edit" && _user_data.phone_type == "other" ? "selected" : "") + ">Other</option>";
	html += "</select>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='address'>Address</label>";
	html += "<div class='col-sm-5'>";
	html += "<input type='text' class='form-control' id='address' value='" + (mode == "edit" ? _user_data.address : "") + "'/>";
	html += "<p class='help-block'>Address</p>";
	html += "</div>";
	html += "<div class='col-sm-4'>";
	html += "<select class='form-control' id='address_type'>";
	html += "<option value='home' " + (mode == "edit" && _user_data.address_type == "home" ? "selected" : "") + ">Home</option>";
	html += "<option value='work' " + (mode == "edit" && _user_data.address_type == "work" ? "selected" : "") + ">Work</option>";
	html += "<option value='custom' " + (mode == "edit" && _user_data.address_type == "custom" ? "selected" : "") + ">Custom</option>";
	html += "<option value='other' " + (mode == "edit" && _user_data.address_type == "other" ? "selected" : "") + ">Other</option>";
	html += "</select>";
	html += "</div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	html += "</div>";
	html += "</div>";
	
	$('#modal_popup_content').html(html);
	
	var footer_html = "";
	footer_html += "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>";
	
	if(mode == "edit") {
		$('#modal_popup_label').text("Edit User");
		footer_html += "<button type='button' class='btn btn-primary' id='btnSaveChanges'>Save Changes</button>";
	} else {
		$('#modal_popup_label').text("Add New User");
		footer_html += "<button type='button' class='btn btn-primary' id='btnCreateUser'>Create</button>";
	}
	
	$('#modal_popup_footer').html(footer_html);
	
	if(mode == "edit") {
		$('#btnSaveChanges').off("click").on("click", function() {
			editUser(_user_data.user_id);
		});
	} else {
		$('#btnCreateUser').off("click").on("click", function() {
			createNewUser();
		});
	}
	
	setGroups();
	
	if(mode == "edit") {
		addUserGroups(_user_data.user_id);
	} else {
		addDefaultGroups();
	}
	
	$('#group_select').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		numberDisplayed: 1,
		enableCaseInsensitiveFiltering: true,
		maxHeight: 400,
		buttonWidth: '400px',
		nonSelectedText: 'Select Group',
		disableIfEmpty: true,
		onDropdownHide: function(event) {
			var selectedList = [];
			$('#group_select option:selected').each(function() {
				selectedList.push($(this).val());
            });

			if(selectedList.length > 0) {
				if(mode =="edit") {
					addGroups(_user_data.user_id, selectedList);
				} else {
					addGroups(null, selectedList);
				}

				$('#group_select').multiselect('deselectAll', false);
                $('#group_select').multiselect('updateButtonText');
			}
        }
	});
}

function setGroups() {
	var html = "";
	
	$.each(group_list, function(key, data) {
		html += "<option value='" + key + "'>" + data.name + "</option>";
	});
	
	$('#group_select').html(html);
}

function isAddedGroup(_group_key) {
	var isExist = false;
	
	$('#added_group_tbody').children("tr").each(function() {
		if($(this).attr("id") == _group_key) {
			isExist = true;
			return;
		}
	});
	
	return isExist;
}

function addGroups(_user_id, _selected_list) {
	var html = "";
	
	for(var i = 0; i < _selected_list.length; i++) {
		var group_info = group_list[_selected_list[i]];
		if(!isAddedGroup(group_info.id)) {
			html += "<tr id='" + group_info.id + "'>";
			html += "<td class='setWidth concat'><div>" + group_info.name + "</div></td>";
			html += "<td class='setWidth concat'><div>" + group_info.email + "</div></td>";
			html += "<td class='setWidth concat'><div>" + group_info.description + "</div></td>";
			html += "<td><button class='btn btn-warning btn-circle btnDeleteAddedGroup'><i class='fa fa-times'></i></button></td>";
			html += "</tr>";
		}
	}
	
	$('#added_group_tbody').append(html);
	
	$('.btnDeleteAddedGroup').off("click").on("click", function() {
		$(this).parent("td").parent("tr").remove();
	});
}

function addDefaultGroups() {
	var html = "";
	
	$.each(group_list, function(_k, _v) {
		var group_info = _v;
		if(local_groups[group_info.id] != null && local_groups[group_info.id].yn_default_group == 'Y') {
			html += "<tr id='" + group_info.id + "'>";
			html += "<td class='setWidth concat'><div>" + group_info.name + "</div></td>";
			html += "<td class='setWidth concat'><div>" + group_info.email + "</div></td>";
			html += "<td class='setWidth concat'><div>" + group_info.description + "</div></td>";
			html += "<td><button class='btn btn-warning btn-circle btnDeleteAddedGroup' disabled><i class='fa fa-times'></i></button></td>";
			html += "</tr>";
		}
	});
	
	$('#added_group_tbody').append(html);
}

function addUserGroups(_user_id) {
	var html = "";
	
	if(local_members[_user_id]) {
		for(var i = 0; i < local_members[_user_id].length; i++) {
			var group_id = local_members[_user_id][i];
			var group_info = group_list[group_id];
			if(group_info) {
				html += "<tr id='" + group_info.id + "'>";
				html += "<td class='setWidth concat'><div>" + group_info.name + "</div></td>";
				html += "<td class='setWidth concat'><div>" + group_info.email + "</div></td>";
				html += "<td class='setWidth concat'><div>" + group_info.description + "</div></td>";
				if(group_info.name == "all") {
					html += "<td><button class='btn btn-warning btn-circle btnDeleteAddedGroup' disabled><i class='fa fa-times'></i></button></td>";				
				} else {
					html += "<td><button class='btn btn-warning btn-circle btnDeleteAddedGroup'><i class='fa fa-times'></i></button></td>";
				}
				
				html += "</tr>";
			}
		}
	}
	
	$('#added_group_tbody').append(html);
	
	$('.btnDeleteAddedGroup').off("click").on("click", function() {
		$(this).parent("td").parent("tr").remove();
	});
}

function getAddedGroups() {
	var group_keys = [];
	$('#added_group_tbody').children("tr").each(function() {
		group_keys.push($(this).attr("id"));
	});
	
	return group_keys;
}

function createNewUser() {
	var first_name = $.trim($('#first_name').val());
	var last_name = $.trim($('#last_name').val());
	var primary_email = $.trim($('#primary_email').val());
	var password = $.trim($('#password').val());
	var password_confirm = $.trim($('#password_confirm').val());
	var secondary_email = $.trim($('#secondary_email').val());
	var secondary_email_type = $.trim($('#secondary_email_type').val());
	var phone = $.trim($('#phone').val());
	var phone_type = $.trim($('#phone_type').val());
	var address = $.trim($('#address').val());
	var address_type = $.trim($('#address_type').val());
	var group_list = getAddedGroups();
	
	if(first_name.length <= 0) {
		alert("First name is empty");
		return false;
	}
	
	if(last_name.length <= 0) {
		alert("Last name is empty");
		return false;
	}
	
	if(primary_email.length <= 0) {
		alert("Primary email is empty");
		return false;
	}
	primary_email += "@" + host_domain;
	
	if(password.length <= 0) {
		alert("Password is empty");
		return false;
	} else if(password != password_confirm) {
		alert("Password is not match");
		return false;
	}
	
	var params = {};
	params.type = "create_user";
	params.first_name = first_name;
	params.last_name = last_name;
	params.primary_email = primary_email;
	params.password = password;
	params.secondary_email = secondary_email;
	params.secondary_email_type = secondary_email_type;
	params.phone = phone;
	params.phone_type = phone_type;
	params.address = address;
	params.address_type = address_type;
	params.group_list = group_list;
	
	$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			google_user_list[ret_data.data.user_id] = ret_data.data;
			
			var ynSuspendedHtml = "<input type='checkbox' class='btnYnSuspendedToggle' " + ((ret_data.data.yn_suspended == "Y") ? "" : "checked") + "/>";
			
			var new_node = list_table.row.add([
				ret_data.data.full_name, 
				ret_data.data.primary_email, 
				ret_data.data.phone, 
				ret_data.data.address,
				ret_data.data.yn_suspended == "Y" ? "Suspended" : "Active",
				ynSuspendedHtml, 
				emailForwardingHtml,
				deleteStaffHtml
			]).draw().node();
			
			$(new_node).attr("id", ret_data.data.user_id);
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = login_url;
		} else {
			alert(ret_data.message);
		}
		
		$('#modal_popup').modal('hide');
	}, "text");
}

function editUser(_user_id) {
	var first_name = $.trim($('#first_name').val());
	var last_name = $.trim($('#last_name').val());
	var primary_email = $.trim($('#primary_email').val());
	var password = $.trim($('#password').val());
	var password_confirm = $.trim($('#password_confirm').val());
	var secondary_email = $.trim($('#secondary_email').val());
	var secondary_email_type = $.trim($('#secondary_email_type').val());
	var phone = $.trim($('#phone').val());
	var phone_type = $.trim($('#phone_type').val());
	var address = $.trim($('#address').val());
	var address_type = $.trim($('#address_type').val());
	var group_list = getAddedGroups();
	
	if(first_name.length <= 0) {
		alert("First name is empty");
		return false;
	}
	
	if(last_name.length <= 0) {
		alert("Last name is empty");
		return false;
	}
	
	if(primary_email.length <= 0) {
		alert("Primary email is empty");
		return false;
	}
	primary_email += "@" + host_domain;
	
	if(password.length > 0) {
		if(password != password_confirm) {
			alert("Password is not match");
			return false;
		}
	}
	
	var inserted_group = [];
	var deleted_group = [];
	if(local_members[_user_id]) {
		inserted_group = _.difference(group_list, local_members[_user_id]);
		deleted_group = _.difference(local_members[_user_id], group_list);
	} else {
		inserted_group = group_list;
	}
	
	var params = {};
	params.user_id = _user_id;
	params.type = "edit_user";
	params.first_name = first_name;
	params.last_name = last_name;
	params.primary_email = primary_email;
	params.password = password;
	params.secondary_email = secondary_email;
	params.secondary_email_type = secondary_email_type;
	params.phone = phone;
	params.phone_type = phone_type;
	params.address = address;
	params.address_type = address_type;
	params.inserted_group = inserted_group;
	params.deleted_group = deleted_group;
	
	$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
		var ret_data = JSON.parse(_data);
		
		if(ret_data.result == "OK") {
			updateRow(ret_data.data);
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = login_url;
		} else {
			alert(ret_data.message);
		}
		
		$('#modal_popup').modal('hide');
	}, "text");
}

function changeUserYnSuspended(_user_id, _yn_suspended) {
	var params = {};
	params.type = "suspend_user";
	params.user_id = _user_id;
	params.yn_suspended = _yn_suspended;
	
	$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			updateRow(ret_data.data);
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = login_url;
		} else {
			list_table.row($('#' + _user_id)).invalidate().draw();
			
			alert(ret_data.message);
		}
	}, "text");
}

function updateRow(_data) {
	google_user_list[_data.user_id] = _data;
	
	var ynSuspendedHtml = "<input type='checkbox' class='btnYnSuspendedToggle' " + ((_data.yn_suspended == "Y") ? "" : "checked") + "/>";
		
	list_table.row($('#' + _data.user_id)).data([
		_data.full_name, 
		_data.primary_email, 
		_data.phone, 
		_data.address,
		_data.yn_suspended == "Y" ? "Suspended" : "Active",
		ynSuspendedHtml, 
		emailForwardingHtml,
		deleteStaffHtml
	]).draw();
}

function deleteStaff(_user_id) {
	var params = {};
	params.type = "delete_user";
	params.user_id = _user_id;
	
	$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			list_table.row($('#' + _user_id)).remove().draw();
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = login_url;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function showEmailForwardingPopup(_user_data) {
	var html = "";
	html += "<div class='row'>";
	html += "<div class='col-lg-12'>";
	html += "<div class='panel panel-default'>";
	html += "<div class='panel-heading'>Synchronize From Google</div>";
	html += "<div class='panel-body'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='alias_email'>";
	html += "<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Email";
	html += "</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='alias_email'/>";
	html += "<p class='help-block'>Email Alias</p>";
	html += "</div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	html += "</div>";
	html += "</div>";
	html += "</div>";
	
	$('#modal_popup_content').html(html);
	
	$('#modal_popup_label').text("Email Forwarding");
	
	var footer_html = "";
	footer_html += "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>";
	footer_html += "<button type='button' class='btn btn-primary' id='btnSave'>Save</button>";
	$('#modal_popup_footer').html(footer_html);
	
	$('#btnSave').off("click").on("click", function() {
		emailForwarding(_user_data);
	});
}

function emailForwarding(_user_data) {
	var alias_email = $.trim($('#alias_email').val());
	if(alias_email.length <= 0) {
		alert("Email address is empty");
		return false;		
	}
	
	var params = {};
	params.type = "email_forwarding";
	params.user_id = _user_data.user_id;
	params.primary_email = _user_data.primary_email;
	params.alias_email = alias_email;
	
	$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			alsert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = login_url;
		} else {
			alert(ret_data.message);
		}
		
		$('#modal_popup').modal('hide');
	}, "text");
}

function getStaffDataByStore(_data) {
	var params = {};
	params.type = "staff_data_by_group";
	params.google_groups_id = _data.google_groups_id;
	
	$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			list_table.clear();
			
			$.each(ret_data.data, function(key, val) {
				var ynSuspendedHtml = "<input type='checkbox' class='btnYnSuspendedToggle' " + (val.yn_suspended == "Y" ? "" : "checked") + " />";
				
				var new_node = list_table.row.add([
					val.full_name,
					val.primary_email,
					val.phone,
					val.address,
					val.yn_suspended == "Y" ? "Suspended" : "Active",
					ynSuspendedHtml,
					emailForwardingHtml,
					deleteStaffHtml
				]).node();

				$(new_node).attr("id", val.user_id);
			});

			list_table.draw();
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = login_url;
		} else {
			alert(ret_data.message);
		}
	}, "text");
}
