<?php
	class CUserManager {
		var $mysql;
		var $google_client;
		var $directory_service;
		
		var $default_option;
		var $google_user_list;
		var $is_google_user_json_format;
		
		var $max_result_count = 500;
		
		function CUserManager($_google_client, $_mysql) {
			$this->google_client = $_google_client;
			$this->directory_service = new Google_Service_Directory($this->google_client);
			$this->mysql = $_mysql;
			
			$this->is_google_user_json_format = false;
			$this->google_user_list = array();
			$this->default_option = array('customer' => 'my_customer', 'projection' => 'full', "orderBy" => "givenName", "sortOrder" => "ASCENDING", "maxResults" => $this->max_result_count);
		}
		
		function getGoogleUserList() {
			return $this->google_user_list;
		}
		
		function getGoogleUserListAsJson() {
			try {
				if(count($this->google_user_list) > 0) {
					if(!$this->is_google_user_json_format) {
						$this->makeGoogleUserListAsJsonFormat();
					}
				
					return json_encode($this->google_user_list);
				} else {
					return "[]";
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get google user list as json; getGoogleUserListAsJson(); ERROR[" . $e->getMessage() . "]");
				return "[]";
			}
		}
		
		function makeGoogleUserListAsJsonFormat() {
			try {
				$users = array();
				foreach($this->google_user_list as $user) {
					$users[$user->getId()] = $this->getFormattedUserData($user);
				}
				
				$this->google_user_list = $users;
				$this->is_google_user_json_format = true;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to make google user list as json format; makeGoogleUserListAsJsonFormat(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function getListGoogleUsers($_option = null) {
			try {
				if(!isset($_option)) {
					$this->google_user_list = array();
					$_option = $this->default_option;
				}
					
				$directory_user = $this->directory_service->users->listUsers($_option);
				$tmp_list = $directory_user->getUsers();
				
				$this->google_user_list = array_merge($this->google_user_list, $tmp_list);
					
				$next_page_token = $directory_user->getNextPageToken();
					
				if(isset($next_page_token) && !empty($next_page_token)) {
					$_option["pageToken"] = $next_page_token;
					$this->getListGoogleUsers($_option);
				} else {
					return;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get google user list; getListGoogleUsers(); ERROR[" . $e->getMessage() . "]");
				return;
			}
		}
		
		function insertUserFromGoogle() {
			try {
				$this->getListGoogleUsers();
				$google_user_list = $this->getGoogleUserList();
					
				$this->mysql->rawQuery("UPDATE google_users SET yn_enable = ?", array("N"));
					
				if(count($google_user_list) > 0) {
					foreach($google_user_list as $user) {
						$formattedUser = $this->getFormattedUserData($user);
						$data = array (
							$formattedUser["user_id"],
							$formattedUser["first_name"],
							$formattedUser["last_name"],
							$formattedUser["full_name"],
							$formattedUser["primary_email"],
							$formattedUser["address"],
							$formattedUser["address_type"],
							$formattedUser["phone"],
							$formattedUser["phone_type"],
							$formattedUser["secondary_email"],
							$formattedUser["secondary_email_type"],
							$formattedUser["yn_suspended"],
							$formattedUser["last_login_date"],
							$formattedUser["created_date"],
							"Y"
						);
				
						$this->mysql->rawQuery("INSERT INTO google_users(google_user_id, first_name, last_name, full_name, primary_email, address, address_type, phone, phone_type, secondary_email, secondary_email_type, 
								yn_suspended, last_login_time, created_time, yn_enable)
							VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
							ON DUPLICATE KEY UPDATE first_name = VALUES(first_name), last_name = VALUES(last_name), full_name = VALUES(full_name), primary_email = VALUES(primary_email),
													yn_suspended = VALUES(yn_suspended), last_login_time = VALUES(last_login_time), created_time = VALUES(created_time), yn_enable = VALUES(yn_enable)", $data);
					}
				}
					
				$this->mysql->rawQuery("DELETE FROM google_users WHERE yn_enable = ?", array("N"));
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert user from google; insertUserFromGoogle(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function createUser($_first_name, $_last_name, $_primary_email, $_password, $_secondary_email, $_secondary_email_type, $_phone, $_phone_type, $_address, $_address_type, $_group_list) {
			try {
				$name = new Google_Service_Directory_UserName();
				$name->setFamilyName($_last_name);
				$name->setGivenName($_first_name);
				$name->setFullName($_first_name . " " . $_last_name);
					
				$new_user = new Google_Service_Directory_User();
				$new_user->setAgreedToTerms(true);
				$new_user->setHashFunction("SHA-1");
				$new_user->setName($name);
				$new_user->setPassword(hash("sha1", $_password));
				$new_user->setChangePasswordAtNextLogin(true);
				$new_user->setPrimaryEmail($_primary_email);
					
				if(isset($_address) && !empty($_address)) {
					$new_address = new Google_Service_Directory_UserAddress();
					$new_address->setFormatted($_address);
					$new_address->setType($_address_type);
					$new_address->setPrimary(true);
						
					$new_user->setAddresses(array($new_address));
				}
					
				if(isset($_phone) && !empty($_phone)) {
					$new_phone = new Google_Service_Directory_UserPhone();
					$new_phone->setValue($_phone);
					$new_phone->setType($_phone_type);
					$new_phone->setPrimary(true);
						
					$new_user->setPhones(array($new_phone));
				}
					
				if(isset($_secondary_email) && !empty($_secondary_email)) {
					$new_email = new Google_Service_Directory_UserEmail();
					$new_email->setAddress($_secondary_email);
					$new_email->setType($_secondary_email_type);
					$new_email->setPrimary(false);
						
					$new_user->setEmails(array($new_email));
				}
					
				$ret_user = $this->directory_service->users->insert($new_user);
				
				if(count($_group_list) > 0) {
					foreach($_group_list as $group) {
						$member = new Google_Service_Directory_Member();
						$member->setId($ret_user->getId());
						$member->setEmail($ret_user->getPrimaryEmail());
						$member->setRole("MEMBER");
						$member->setType("USER");
							
						$ret_member = $this->directory_service->members->insert($group, $member);
					}
				}
				
				return $this->getFormattedUserData($ret_user);
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to create google user; createUser(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function updateUser($_user_id, $_first_name, $_last_name, $_primary_email, $_password, $_secondary_email, $_secondary_email_type, $_phone, $_phone_type, $_address, $_address_type, $_inserted_group, $_deleted_group) {
			try {
				$name = new Google_Service_Directory_UserName();
				$name->setFamilyName($_last_name);
				$name->setGivenName($_first_name);
				$name->setFullName($_first_name . " " . $_last_name);
					
				$new_user = new Google_Service_Directory_User();
				
				$new_user->setName($name);
				$new_user->setPrimaryEmail($_primary_email);
				
				if(isset($_password) && !empty($_password)) {
					$new_user->setHashFunction("SHA-1");
					$new_user->setPassword(hash("sha1", $_password));
				}
					
				if(isset($_address) && !empty($_address)) {
					$new_address = new Google_Service_Directory_UserAddress();
					$new_address->setFormatted($_address);
					$new_address->setType($_address_type);
					$new_address->setPrimary(true);
					
					$new_user->setAddresses(array($new_address));
				}
					
				if(isset($_phone) && !empty($_phone)) {
					$new_phone = new Google_Service_Directory_UserPhone();
					$new_phone->setType($_phone_type);
					$new_phone->setValue($_phone);
					$new_phone->setPrimary(true);
					
					$new_user->setPhones(array($new_phone));
				}
					
				if(isset($_secondary_email) && !empty($_secondary_email)) {
					$new_email = new Google_Service_Directory_UserEmail();
					$new_email->setAddress($_secondary_email);
					$new_email->setType($_secondary_email_type);
					$new_email->setPrimary(false);
					
					$new_user->setEmails(array($new_email));
				}
					
				$ret_user = $this->directory_service->users->update($_user_id, $new_user);
					
				if(count($_deleted_group) > 0) {
					foreach($_deleted_group as $group) {
						$ret_member = $this->directory_service->members->delete($group, $_user_id);
					}
				}
				
				if(count($_inserted_group) > 0) {
					foreach($_inserted_group as $group) {
						$member = new Google_Service_Directory_Member();
						$member->setId($ret_user->getId());
						$member->setEmail($ret_user->getPrimaryEmail());
						$member->setRole("MEMBER");
						$member->setType("USER");
					
						$ret_member = $this->directory_service->members->insert($group, $member);
					}
				}
	
				return $this->getFormattedUserData($ret_user);
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update google user; updateUser(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function suspendUser($_user_id, $_yn_suspended) {
			try {
				$suspend_user = new Google_Service_Directory_User();
				
				$suspend_user->setSuspended(false);
				if($_yn_suspended == "Y") {
					$suspend_user->setSuspended(true);
				}
				
				$ret_user = $this->directory_service->users->update($_user_id, $suspend_user);
				
				return $this->getFormattedUserData($ret_user);
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to suspend google user; suspendUser(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function deleteUser($_user_id) {
			try {
				$ret = $this->directory_service->users->delete($_user_id);
				return true;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to suspend google user; suspendUser(); ERROR[" . $e->getMessage() . "]");
		
				return false;
			}
		}
		
		function emailForwarding($_user_id, $_primary_email, $_alias_email) {
			try {
				list($username, $domain) = split('@', $_primary_email);
					
				$token = json_decode($this->google_client->getAccessToken());
					
				$ret_email_forwarding = setEmailForwarding($domain, $username, $_alias_email, $token);
					
				if($ret_email_forwarding["ret_code"] != 200) {
					throw new Exception($ret_email_forwarding["ret_msg"]);
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to forward email; emailForwarding(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function getUsersByGroupId($_google_groups_id) {
			try {
				$users = $this->mysql->rawQuery("SELECT u.google_user_id AS user_id, u.first_name, u.last_login_time, u.full_name, u.primary_email, u.phone, u.phone_type, u.address, u.address_type, u.secondary_email, u.secondary_email_type, 
													u.yn_suspended, u.last_login_time, u.created_time 
												FROM google_members m JOIN google_users u ON m.google_user_id = u.google_user_id 
												WHERE m.google_groups_id = ?;", array($_google_groups_id));
				
				$retUser = array();
				
				if(isset($users)) {
					foreach ($users as $user_row) {
						$retUser[$user_row["user_id"]] = $user_row;
					}
				}
				
				return $retUser;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get user by group id; getUsersByGroupId(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function getFormattedUserData($_user) {
			$user_phone = "";
			$user_phone_type = "";
			$user_phones = $_user->getPhones();
			if(isset($user_phones) && count($user_phones) > 0) {
				foreach ($user_phones as $user_phone_row) {
					if($user_phone_row["primary"] == 1) {
						$user_phone = $user_phone_row["value"];
						$user_phone_type = $user_phone_row["type"];
						break;
					}
				}
			}
				
			$user_address = "";
			$user_address_type = "";
			$user_addresses = $_user->getAddresses();
			if(isset($user_addresses) && count($user_addresses) > 0) {
				foreach ($user_addresses as $user_addresse_row) {
					if($user_addresse_row["primary"] == 1) {
						$user_address = $user_addresse_row["formatted"];
						$user_address_type = $user_addresse_row["type"];
					}
				}
			}
				
			$secondary_email = "";
			$secondary_email_type = "";
			$secondary_emails = $_user->getEmails();
			if(isset($secondary_emails) && count($secondary_emails) > 0) {
				foreach ($secondary_emails as $secondary_email_row) {
					if(!isset($secondary_email_row["primary"]) && isset($secondary_email_row["type"])) {
						$secondary_email = $secondary_email_row["address"];
						$secondary_email_type = $secondary_email_row["type"];
						break;
					}
				}
			}
			
			$retUser = array();
			$retUser["user_id"] = $_user->getId();
			$retUser["first_name"] = $_user->getName()->getGivenName();
			$retUser["last_name"] = $_user->getName()->getFamilyName();
			$retUser["full_name"] = $_user->getName()->getFullName();
			$retUser["primary_email"] = $_user->getPrimaryEmail();
			$retUser["phone"] = $user_phone;
			$retUser["phone_type"] = $user_phone_type;
			$retUser["address"] = $user_address;
			$retUser["address_type"] = $user_address_type;
			$retUser["secondary_email"] = $secondary_email;
			$retUser["secondary_email_type"] = $secondary_email_type;
			$retUser["yn_suspended"] = $_user->getSuspended() ? "Y" : "N";
			$retUser["last_login_date"] = $_user->getLastLoginTime();
			$retUser["created_date"] = $_user->getCreationTime();
			
			return $retUser;
		}
	}
?>