<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../../inc/config.php");
		require_once_autoload();
		require_once_classes(array("CSession", "CGoogleClientManager"));
	
		$session = new CSession();
	
		if($session->isLogin == false) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$google_client = new CGoogleClientManager($session->getAccessToken());
		if($google_client->isAccessTokenExpired()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
	} catch (Exception $e) {
		qbw_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > Settings > Synchronize with Google"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/moment.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/transition.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/collapse.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			$(function() {
				$('#btnSyncWithGoogle').off("click").on("click", function() {
					var result = confirm("Are you sure?");
					if(result) {
						var params = {};
						params.type = "sync_with_google";
						$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
							var ret_data = JSON.parse(_data);
							if(ret_data.result == "OK") {
								alert("Done");
							} else if(ret_data.result == "NOT_LOGIN") {
								location.href = "<?php echo CONF_URL_LOGIN; ?>";
							} else {
								alert(ret_data.message);
							}
						}, "text");
					}

					return false;
				});
			});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Settings > Synchronize with Google</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">Synchronize with Google data</div>
							<div class="panel-body">
								<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modal_popup' id='btnSyncWithGoogle'>Sync with Google</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>