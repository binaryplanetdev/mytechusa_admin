<?php
	class CGoogleClientManager {
		var $client = null;
		
		function CGoogleClientManager($_access_token = null) {
			try {
				$this->client = new Google_Client();
					
				$this->initClient();
					
				$this->setAccessToken($_access_token);
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to create google client; CGoogleClientManager();");
			}
		}
		
		function initClient() {
			try {
				$this->client->setAuthConfigFile(CONF_PATH_GOOGLE_CLIENT_SECRET);
				$this->client->setHostedDomain(HOST_DOMAIN);
					
				$this->setClientScope();
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to init google client; initClient();");
			}
		}
		
		function setClientScope() {
			try {
				$this->client->addScope(Google_Service_Directory::ADMIN_DIRECTORY_GROUP);
				$this->client->addScope(Google_Service_Directory::ADMIN_DIRECTORY_USER);
				$this->client->addScope(Google_Service_Directory::ADMIN_DIRECTORY_GROUP_MEMBER);
				$this->client->addScope(Google_Service_Directory::ADMIN_DIRECTORY_USER_ALIAS);
				$this->client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
				$this->client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
				$this->client->addScope("https://apps-apis.google.com/a/feeds/emailsettings/2.0/");
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to set client scope; setClientScope();");
			}
		}
		
		function setAccessToken($_access_token) {
			if($_access_token != null) {
				$this->client->setAccessToken($_access_token);
			}
		}
		
		function getAccessToken() {
			return $this->client->getAccessToken();
		}
		
		function createAuthUrl() {
			return $this->client->createAuthUrl();
		}
		
		function authenticate($_code) {
			$this->client->authenticate($_code);
		}
				
		function checkPermission() {
			try {
				if($this->getDirectoryUser()) {
					return true;
				} else {
					bc_error("PERMISSION DENIED!");
					return false;
				}
			} catch (Exception $e) {
				bc_error("PERMISSION DENIED!");
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to check permission; checkPermission();");
				return false;
			}
		}
		
		function getDirectoryUser() {
			try {
				$directory_service = new Google_Service_Directory($this->client);
				
				$google_account = $this->getGoogleAccountFromOauth();
				if(!isset($google_account)) {
					return false;
				}
				
				$directory_user = $directory_service->users->get($google_account->getId());
				if(isset($directory_user) && $directory_user->getIsAdmin() == 1) {
					return true;
				}
				
				return false;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get google directory user; getDirectoryUser();");
				return false;
			}
		}
		
		function getGoogleAccountFromOauth() {
			try {
				$google_oauth = new Google_Service_Oauth2($this->client);
				$google_account = $google_oauth->userinfo->get();
					
				if(!isset($google_account)) {
					bc_error("PERMISSION DENIED!");
					return null;
				}
					
				$hostDomain = $google_account->getHd();
				
				if(!isset($hostDomain) || empty($hostDomain)) {
					bc_error("PERMISSION DENIED!");
					return null;
				}
				
				if($hostDomain != HOST_DOMAIN) {
					bc_error("PERMISSION DENIED!");
					return null;
				}
					
				return $google_account;
			} catch (Exception $e) {
				bc_error("PERMISSION DENIED!");
				
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get google account from oauth; getGoogleAccountFromOauth();");
				
				return null;
			}
		}
		
		function isAccessTokenExpired() {
			return $this->client->isAccessTokenExpired();
		}
		
		function getRefreshToken() {
			return $this->client->getRefreshToken();
		}
		
		function getGoogleClient() {
			return $this->client;
		}
	}
?>