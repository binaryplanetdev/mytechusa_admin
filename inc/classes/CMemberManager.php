<?php
	class CMemberManager {
		var $mysql;
		var $google_client;
		var $directory_service;
		
		var $local_member_list;
		var $google_member_list;
		
		var $default_option;
		var $max_result_count = 500;
		
		function CMemberManager($_google_client, $_mysql) {
			$this->google_client = $_google_client;
			$this->directory_service = new Google_Service_Directory($this->google_client);
			$this->mysql = $_mysql;
			
			$this->google_member_list = array();
			$this->local_member_list = array();
			$this->default_option = array("maxResults" => $this->max_result_count);
		}
		
		function getLocalMemberList() {
			return $this->local_member_list;
		}
		
		function getLocalMemberListAsJson() {
			if(count($this->local_member_list) > 0) {
				return json_encode($this->local_member_list);
			} else {
				return "[]";
			}
		}
		
		function getListLocalMembers() {
			try {
				$this->local_member_list = array();
				
				$local_members = $this->mysql->get("google_members");
					
				if($this->mysql->count <= 0) {
					return;
				}
					
				foreach ($local_members as $member) {
					if(!isset($this->local_member_list[$member["google_user_id"]])) {
						$this->local_member_list[$member["google_user_id"]] = array();
					}
				
					$this->local_member_list[$member["google_user_id"]][] = $member["google_groups_id"];
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get local member list; getListLocalMembers(); ERROR[" . $e->getMessage() . "]");
			}
		}
		
		function getGoogleMemberList() {
			return $this->google_member_list;
		}
		
		function getListGoogleMembers($_group_id, $_option = null) {
			try {
				if(!isset($_option)) {
					$this->google_member_list = array();
					$_option = $this->default_option;
				}
					
				$directory_member = $this->directory_service->members->listMembers($_group_id, $_option);
				$tmp_members = $directory_member->getMembers();
					
				$this->google_member_list = array_merge($this->google_member_list, $tmp_members);
					
				$next_page_token = $directory_member->getNextPageToken();
					
				if(isset($next_page_token) && !empty($next_page_token)) {
					$_option["pageToken"] = $next_page_token;
					$this->getListGoogleMembers($_option);
				} else {
					return;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get google member list; getListGoogleMembers(); ERROR[" . $e->getMessage() . "]");
				return;
			}
		}
		
		function insertMemberFromGoogle($_google_group_list) {
			try {
				$this->mysql->delete("google_members");
				
				if(count($_google_group_list) > 0) {
					foreach($_google_group_list as $group) {
						$group_id = $group->getId();
						
						$this->getListGoogleMembers($group_id);
							
						$google_member_list = $this->getGoogleMemberList();
							
						if(count($google_member_list) > 0) {
							foreach($google_member_list as $member) {
								$data = Array (
									"google_groups_id" => $group_id,
									"google_user_id" => $member->getId(),
									"google_user_email" => $member->getEmail(),
									"member_role" => $member->getRole(),
									"member_type" => $member->getType()
								);
				
								$id = $this->mysql->insert('google_members', $data);
							}
						}
					}
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert member from google; insertMemberFromGoogle(); ERROR[" . $e->getMessage() . "]");
			}
		}
	}
?>