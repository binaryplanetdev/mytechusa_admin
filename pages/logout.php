<?php 
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession"));
	
		$session = new CSession();
	
		$session->logout();
		
		$redirect_url = CONF_SERVER_HOST . CONF_URL_INDEX;
		
		if(isset($_GET["redirect"]) && !empty($_GET["redirect"])) {
			$redirect = CONF_SERVER_HOST . $_GET["redirect"];
		}
		
		header("Location: https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=" . $redirect);
		exit;
		
	} catch (Exception $e) {
		qbw_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}	
?>
