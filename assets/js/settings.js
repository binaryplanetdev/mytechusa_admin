function changeDefaultStatus(_group_id, _yn_default) {
	var params = {};
	params.type = "change_group_default";
	params.group_id = _group_id;
	params.yn_default = _yn_default;
	$.post("/GoogleApi/ajax/ajax.php", params, function(_data) {
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			local_group_list[_group_id].yn_default_group = _yn_default;
			
			var ynDefaultHtml = "<input type='checkbox' class='btnYnDefaultToggle' " + (local_group_list[_group_id].yn_default_group == "Y" ? "checked" : "") + "/>";
			
			list_table.row($('#' + _group_id)).data([
				local_group_list[_group_id].group_name,
				local_group_list[_group_id].group_email,
				local_group_list[_group_id].description,
				local_group_list[_group_id].direct_members_count,
				local_group_list[_group_id].yn_default_group == "Y" ? "default" : "none",
				ynDefaultHtml
         	]).draw();
			
			alert("OK");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = login_url;
		} else {
			list_table.row($('#' + _group_id)).invalidate().draw();
			alert(ret_data.message);
		}
		
		$('#modal_popup').modal('hide');
	}, "text");
}