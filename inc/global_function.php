<?php
	/**
	 * require_once_autoload(Array("CUtil"));
	 */
	function require_once_autoload() {
		set_include_path(CONF_PATH_CLASS);
		require_once(CONF_PATH_CLASS . "Google/autoload.php");
	}

	/**
	 * require_once_classes(Array("CUtil"));
	 */
	function require_once_classes($_classNameArr) {
		foreach ($_classNameArr AS $className) {
			require_once(CONF_PATH_CLASS . $className . ".php");
		}
	}

	/**
	 * debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "can not connect to memcached");
	 */
	function debug_mesg($_type, $_class, $_function, $_line, $_mesg) {
		if (!CONF_DEBUG) {
			return;
		}
	
		if ((strcmp($_type, "E") == 0) && !CONF_DEBUG_ERROR) {
			return;
		} else if ((strcmp($_type, "I")) && !CONF_DEBUG_INFO) {
			return;
		}
	
		$_mesg .= "@1-". whereCalled(1);
		$_mesg .= "@2-". whereCalled(2);
		$_mesg .= "@3-". whereCalled(3);
		
		if (CONF_DEBUG_DISPLAY) {
			printf("%s|%s|%s|%s|%d|%s<br/>\r\n", date("Y-m-d H:i:s"), $_type, $_class, $_function, $_line, substr($_mesg, 0, 51200));
		}
	
		if (CONF_DEBUG_SAVE) {
			$save_file = CONF_DEBUG_SAVE_FILE_ETC;
			if ($_type == "E") {
				$save_file = CONF_DEBUG_SAVE_FILE_ERROR;
			}
			
			$f = sprintf("%s%d_%s", CONF_PATH_DEBUG_FILE, date("YmdH"), $save_file);
	
			$is_new = true;
			if (file_exists($f)) {
				$is_new = false;
			}
	
			$fp = fopen($f, "a");
	
			if ($is_new) {
				chmod($f, 0666);
			}
	
			if ($fp) {
				printf("%s|%s|%s|%s|%d|%s\r\n", date("Y-m-d H:i:s"), $_type, $_class, $_function, $_line, substr($_mesg, 0, 51200));
				fclose($fp);
			}
		}
	}

	function whereCalled($_level = 1) {
		$trace = debug_backtrace();
		
		if (!isset($trace[$_level])) {
			return "nothing";
		}
			
		$file = $trace[$_level]["file"];
		$line = $trace[$_level]["line"];
		$object = isset($trace[$_level]["object"]) ? $trace[$_level]["object"] : null;
		
		$args = "";
		if ($trace[$_level]["args"]) {
			unSet($trace[$_level]["args"]["_query"]);
			$args = serialize($trace[$_level]["args"]);
		}
	
		if (is_object($object)) {
			$object = get_class($object);
		}
	
		return "Where called [" . $_level . "]: line " . $line . " of " . $object . " with args[" . $args . "] (in " . $file . ")";
	}

	function bc_return($_result = "OK", $_message = "SUCCESS", $_data = null) {
	    return json_encode(Array("result" => $_result, "message" => $_message, "data" => $_data));
	}
	
	function bc_error($_message) {
		global $_BC_ERROR;
	
		$_BC_ERROR = Array('message' => $_message);
	}
	
	function moveToSpecificPage($_url, $_redirect_url = null) {
		if(isset($_redirect_url) && !empty($_redirect_url)) {
			$_url = "?redirect=" . $_redirect_url;
		}
		
		header('Location: ' . filter_var($_url, FILTER_SANITIZE_URL));
	}
	
	function getXMLForSetEmailForwarding($_email_forward_to) {
		$result = "<?xml version='1.0' encoding='utf-8'?>
					<atom:entry xmlns:atom='http://www.w3.org/2005/Atom' xmlns:apps='http://schemas.google.com/apps/2006'>
						<apps:property name='enable' value='true' />
						<apps:property name='forwardTo' value='" . $_email_forward_to . "' />
						<apps:property name='action' value='KEEP' />
					</atom:entry>";
	
		return $result;
	}
	
	function setEmailForwarding($_domain, $_user_name, $email_forward_to, $_token) {
		$ch = curl_init();
		$url_feed = "https://apps-apis.google.com/a/feeds/emailsettings/2.0/".$_domain."/".$_user_name."/forwarding";
	
		curl_setopt($ch, CURLOPT_URL, $url_feed);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Authorization: ' . $_token->token_type . ' ' . $_token->access_token,
				'Content-type: application/atom+xml'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, getXMLForSetEmailForwarding($email_forward_to));
	
		$result = curl_exec($ch);
		$info = curl_getinfo($ch);
		$headers = get_headers_from_curl_response($result);
		$error_code = curl_errno($ch);
		$error_msg = curl_error($ch);
		curl_close($ch);
		
// 		echo "<pre>result";
// 		print_r($result);
// 		echo "<pre>error_code";
// 		print_r($error_code);
// 		echo "<pre>error_msg";
// 		print_r($error_msg);
	
		$ret = array("ret_code" => $info["http_code"], "ret_msg" => $headers["http_code"]);
	
		return $ret;
	}
		
	function get_headers_from_curl_response($response) {
		$headers = array();
	
		$header_text = substr($response, 0, strpos($response, "\r\n\r\n"));
	
		foreach (explode("\r\n", $header_text) as $i => $line) {
			if ($i === 0) {
				$headers['http_code'] = $line;
			} else {
				list ($key, $value) = explode(': ', $line);
				$headers[$key] = $value;
			}
		}
	
		return $headers;
	}
	
	function buildTree($items) {
		$childs = array();
			
		foreach($items as &$item) {
			$childs[$item['parent_id']][] = &$item;
			unset($item);
		}

		foreach($items as &$item) {
			if (isset($childs[$item['store_pk']])) {
				$item['nodes'] = $childs[$item['store_pk']];
			}
		}
	
		return $childs[0];
	}
?>
