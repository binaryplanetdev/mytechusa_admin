<?php
	class CSession {
		var $isLogin = false;
		var $accessToken = null;
		
		function CSession() {
			$this->loginFromSession();
		}
		
		function loginFromSession() {
			if (isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) {
				$this->login($_SESSION['access_token']);
			} else {
				$this->logout();
			}
		}
		
		function login($_access_token) {
			if(isset($_access_token) && !empty($_access_token)) {
				$_SESSION['access_token'] = $_access_token;
				$this->accessToken = $_access_token;
				$this->isLogin = true;
			}
		}
		
		function logout() {
			unset($_SESSION['access_token']);
			$this->accessToken = null;
			$this->isLogin = false;
		}
		
		function getAccessToken() {
			$this->loginFromSession();
			
			return $this->accessToken;
		}
	}
?>